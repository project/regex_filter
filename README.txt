Overview:
--------
regex_filters implements perl/Php regular expression filters.


Installation and configuration:
------------------------------
Installation is as simple as copying the module into your 'modules' directory,
then enabling the module at 'administer >> modules'.  

To create new filters, go to 'administer >> settings >> regex_filters'.

Requires:
--------
 - Drupal 5.X


Credits:
-------
 - Written by Kenn Persinger <lestat@iglou.com>
